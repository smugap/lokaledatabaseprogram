﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lokale
{
    public partial class Form3 : Form
    {
        public delegate void addTo(string a, string b, string c, string d, string e, string f);

        private addTo c = null;
        public Form3(addTo test)
        {
            InitializeComponent();
            c = test;
        }
        public void setText(string a, string b, string c, string d, string e, string f)
        {
            textBox1.Text = a;
            textBox2.Text = b;
            textBox3.Text = c;
            textBox4.Text = d;
            textBox5.Text = e;
            textBox6.Text = f;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            c(textBox1.Text, textBox2.Text, textBox3.Text,textBox4.Text,textBox5.Text, textBox6.Text);
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            this.Close();
        }
    }
}
