﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Lokale
{
    public partial class Form5 : Form
    {
        private MySqlConnection _databaseConnection;
        string _connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=lokale;";


        public Form5()
        {
            InitializeComponent();
            
            this.FormClosing += Form5_FormClosing1;
        }

        private void Form5_FormClosing1(object sender, FormClosingEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            
            string query = $"SELECT * FROM `worker` WHERE worker.login='{textBox1.Text}' AND worker.password = '{textBox2.Text}';";
            _databaseConnection = new MySqlConnection(_connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) {CommandTimeout = 60};
            MySqlDataReader reader;
            try
            {
                _databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {

                    //while (reader.Read())
                    //{

                    //}
                    this.Hide();
                    var a = new Form1();
                    a.ShowDialog();
                    this.Close();
                }
                else
                {
                    Console.WriteLine("No rows found.");
                    MessageBox.Show("You cannot login, please, try again!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                _databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void Form5_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }
    }
}
