﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using MySql.Data.MySqlClient;

namespace Lokale
{
    public partial class Form1 : Form
    {
        private MySqlConnection _databaseConnection;
        
        public Form1()
        {
            //Starting application you have to add MySql.Data from Nuget
            //additional you have to create database in mysql using for instance xampp
            InitializeComponent();
            
            //Form5 login = new Form5(close);
            //login.ShowDialog();

            //fix first commit

            refresh();
              
            listView1.View = View.Details;
            listView1.FullRowSelect = true;
            listView2.View = View.Details;
            listView2.FullRowSelect = true;
            listView3.View = View.Details;
            listView3.FullRowSelect = true;
            listView4.View = View.Details;
            listView4.FullRowSelect = true;

            listView1.Columns.Add("ID", 50);
            listView1.Columns.Add("Ulica", 200);
            listView1.Columns.Add("Nr", 70);
            listView2.Columns.Add("ID", 30);
            listView2.Columns.Add("Imie", 50);
            listView2.Columns.Add("Nazwisko", 70);
            listView2.Columns.Add("Telefon", 70);
            listView2.Columns.Add("Email", 90);
            listView2.Columns.Add("Naleznosc", 60);
            listView3.Columns.Add("ID", 30);
            listView3.Columns.Add("Nr", 50);
            listView3.Columns.Add("Metraz", 90);
            listView3.Columns.Add("Cena /m2", 70);
            listView3.Columns.Add("Id mieszkanca", 70);
            listView3.Columns.Add("Id budynku", 70);
            listView4.Columns.Add("ID", 30);
            listView4.Columns.Add("Imie", 50);
            listView4.Columns.Add("Nazwisko", 70);
            listView4.Columns.Add("Telefon", 70);
            listView4.Columns.Add("Email", 90);
            listView4.Columns.Add("Naleznosc", 60);
            if (listView1.Items.Count > 0)
            {
                listView1.Items[0].Selected = true;
                listView1.Select();
            }
            


        }

        public void close()
        {
            this.Close();
        }
      
        public void refresh()
        {
            string query = "SELECT * FROM budynki;";
            string query2 = "SELECT * FROM mieszkancy;";
            string query3 = "SELECT * FROM mieszkania;";
            string query4 = "SELECT * FROM mieszkancy WHERE mieszkancy.naleznosc > 0;";
            _databaseConnection = new MySqlConnection(Settings._connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };
            MySqlDataReader reader;
            try
            {
                _databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    listView1.Items.Clear();
                    while (reader.Read())
                    {
                        string[] items = { reader.GetString(0), reader.GetString(1), reader.GetString(2) };
                        ListViewItem item = new ListViewItem(items);
                        listView1.Items.Add(item);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                commandDatabase = new MySqlCommand(query2, _databaseConnection) { CommandTimeout = 60 };
                reader.Dispose();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    listView2.Items.Clear();
                    while (reader.Read())
                    {
                        string[] items =
                        {
                            reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3),
                            reader.GetString(4), reader.GetString(5)
                        };
                        ListViewItem item = new ListViewItem(items);
                        listView2.Items.Add(item);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                commandDatabase = new MySqlCommand(query3, _databaseConnection) { CommandTimeout = 60 };
                reader.Dispose();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    listView3.Items.Clear();
                    while (reader.Read())
                    {
                        string[] items =
                        {
                            reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3),
                            reader.GetString(4), reader.GetString(5)
                        };
                        ListViewItem item = new ListViewItem(items);
                        listView3.Items.Add(item);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                commandDatabase = new MySqlCommand(query4, _databaseConnection) { CommandTimeout = 60 };
                reader.Dispose();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    listView4.Items.Clear();
                    while (reader.Read())
                    {
                        string[] items =
                        {
                            reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3),
                            reader.GetString(4), reader.GetString(5)
                        };
                        ListViewItem item = new ListViewItem(items);
                        listView4.Items.Add(item);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                _databaseConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void addItem(string a, string b, string c)
        {
            string[] items = {a, b, c};
            ListViewItem item = new ListViewItem(items);
            listView1.Items.Add(item);
            string query = $"INSERT INTO budynki(`id_budynku`, `ulica`, `nr_budynku`) VALUES ( {a},'{b}',{c});";

            _databaseConnection = new MySqlConnection(Settings._connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) {CommandTimeout = 60};

            try
            {
                _databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                _databaseConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }
        private void addItem2(string a, string b, string c,string d,string e,string f)
        {
            string[] items = { a, b, c ,d,e,f};
            ListViewItem item = new ListViewItem(items);
            listView2.Items.Add(item);
            //string query = $"INSERT INTO budynki(`id_budynku`, `ulica`, `nr_budynku`) VALUES ( {a},'{b}',{c})";
            string query = $"INSERT INTO mieszkancy(`id_mieszkanca`, `imie`, `nazwisko`, `tel`, `email`, `naleznosc`) VALUES('{a}', '{b}', '{c}', '{d}', '{e}', '{f}');";
            _databaseConnection = new MySqlConnection(Settings._connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };

            try
            {
                _databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                _databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        private void addItem3(string a, string b, string c, string d, string e, string f)
        {
            string[] items = { a, b, c, d, e, f };
            ListViewItem item = new ListViewItem(items);
            listView3.Items.Add(item);
            string query =
                $"INSERT INTO `mieszkania` (`id_mieszkania`, `nr_mieszkania`, `metraz`, `cena_za_metr`, `id_mieszkanca`, `id_budynku`) VALUES('{a}', '{b}', '{c}', '{d}', '{e}', '{f}');";
            _databaseConnection = new MySqlConnection(Settings._connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };

            try
            {
                _databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                _databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        private void update(string a,string b, string c)
        {
            try
            {
                listView1.SelectedItems[0].SubItems[0].Text = a;
                listView1.SelectedItems[0].SubItems[1].Text = b;
                listView1.SelectedItems[0].SubItems[2].Text = c;
            }
            catch (Exception e)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string query = $"UPDATE `budynki` SET `id_budynku` = '{a}', `ulica` = '{b}', `nr_budynku` = '{c}' WHERE `budynki`.`id_budynku` = {listView1.SelectedItems[0].Text};";
        
            MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };

            try
            {
                _databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                _databaseConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            refresh();
        }
        private void update2(string a, string b, string c,string d,string e,string f)
        {
            try
            {
                listView2.SelectedItems[0].SubItems[0].Text = a;
                listView2.SelectedItems[0].SubItems[1].Text = b;
                listView2.SelectedItems[0].SubItems[2].Text = c;
                listView2.SelectedItems[0].SubItems[3].Text = d;
                listView2.SelectedItems[0].SubItems[4].Text = e;
                listView2.SelectedItems[0].SubItems[5].Text = f;
            }
            catch (Exception exception)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string query = $"UPDATE `mieszkancy` SET `id_mieszkanca` = '{a}', `imie` = '{b}', `nazwisko` = '{c}', `tel` = '{d}', `email` = '{e}', `naleznosc` = '{f}' WHERE `mieszkancy`.`id_mieszkanca` = {listView2.SelectedItems[0].Text};";
            MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };

            try
            {
                _databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                _databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            refresh();
        }
        private string aa;
        private string ff;
        private string ee;
        private void update3(string a, string b, string c, string d, string e, string f)
        {
            try
            {
                listView3.SelectedItems[0].SubItems[0].Text = a;
                listView3.SelectedItems[0].SubItems[1].Text = b;
                listView3.SelectedItems[0].SubItems[2].Text = c;
                listView3.SelectedItems[0].SubItems[3].Text = d;
                listView3.SelectedItems[0].SubItems[4].Text = e;
                listView3.SelectedItems[0].SubItems[5].Text = f;
            }
            catch (Exception exception)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //string query = $"UPDATE `budynki` SET `id_budynku` = '{a}', `ulica` = '{b}', `nr_budynku` = '{c}' WHERE `budynki`.`id_budynku` = {listView1.SelectedItems[0].Text};";
            //string query =
              //  $"UPDATE `mieszkancy` SET `id_mieszkanca` = '{a}', `imie` = '{b}', `nazwisko` = '{c}', `tel` = '{d}', `email` = '{e}', `naleznosc` = '{f}' WHERE `mieszkancy`.`id_mieszkanca` = {listView3.SelectedItems[0].Text};";

            string query =
                $"UPDATE `mieszkania` SET `id_mieszkania` = '{a}', `nr_mieszkania` = '{b}', `metraz` = '{c}', `cena_za_metr` = '{d}', `id_mieszkanca` = '{e}', `id_budynku` = '{f}' WHERE `mieszkania`.`id_mieszkania` = '{aa}' AND `mieszkania`.`id_mieszkanca` = '{ee}' AND `mieszkania`.`id_budynku` = '{ff}';";
            MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) {CommandTimeout = 60};


            try
            {
                _databaseConnection.Open();
                MySqlDataReader myReader = commandDatabase.ExecuteReader();
                _databaseConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            refresh();
        }
        private void delete()
        {
            ListViewItem test;
            int help;
            try
            {
                help = listView1.SelectedIndices[0];
                test = listView1.SelectedItems[0];
            }
            catch (Exception e)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (MessageBox.Show("Are you sure?", "DELETE", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) ==
                DialogResult.OK)
            {
               

                listView1.Items.RemoveAt(help);
                string query = $"DELETE FROM budynki WHERE budynki.id_budynku = {test.Text};";
                //_databaseConnection = new MySqlConnection(_connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };

                try
                {
                    _databaseConnection.Open();
                    MySqlDataReader myReader = commandDatabase.ExecuteReader();
                    _databaseConnection.Close();
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
            }
        }
        private void delete2()
        {
            int help;
            ListViewItem test;
            try
            {
                help = listView2.SelectedIndices[0];
                test = listView2.SelectedItems[0];
            }
            catch (Exception e)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (MessageBox.Show("Are you sure?", "DELETE", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) ==
                DialogResult.OK)
            {
                

                listView2.Items.RemoveAt(help);
                string query = $"DELETE FROM mieszkancy WHERE mieszkancy.id_mieszkanca = {test.Text};";
                //_databaseConnection = new MySqlConnection(_connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };

                try
                {
                    _databaseConnection.Open();
                    MySqlDataReader myReader = commandDatabase.ExecuteReader();
                    _databaseConnection.Close();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
        }
        private void delete3()
        {
            int help;
            ListViewItem test;
            try
            {
                help = listView3.SelectedIndices[0];
                test = listView3.SelectedItems[0];
            }
            catch (Exception e)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show("Are you sure?", "DELETE", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) ==
                DialogResult.OK)
            {
               

                listView3.Items.RemoveAt(help);
                string query = $"DELETE FROM mieszkania WHERE mieszkania.id_mieszkania = {test.Text};";
                //_databaseConnection = new MySqlConnection(_connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };

                try
                {
                    _databaseConnection.Open();
                    MySqlDataReader myReader = commandDatabase.ExecuteReader();
                    _databaseConnection.Close();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //add
            Form2 form = new Form2(addItem);
            form.ShowDialog();
            refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //delete
            delete();
            refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //edit
            Form2 form = new Form2(update);
            try
            {
                form.setText(listView1.SelectedItems[0].SubItems[0].Text, listView1.SelectedItems[0].SubItems[1].Text, listView1.SelectedItems[0].SubItems[2].Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            form.ShowDialog();
            refresh();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //add mieszkancy
            //

            Form3 form = new Form3(addItem2);
            form.ShowDialog();
            refresh();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //delete mieszkancy
            delete2();
            refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //edit mieszkancy
            Form3 form = new Form3(update2);
            try
            {
                form.setText(listView2.SelectedItems[0].SubItems[0].Text, listView2.SelectedItems[0].SubItems[1].Text, listView2.SelectedItems[0].SubItems[2].Text,
                    listView2.SelectedItems[0].SubItems[3].Text, listView2.SelectedItems[0].SubItems[4].Text, listView2.SelectedItems[0].SubItems[5].Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            form.ShowDialog();
            refresh();
        }

        private void button9_Click(object sender, EventArgs e)
        {

            //add
            Form4 form = new Form4(addItem3,_databaseConnection);
            form.ShowDialog();
            refresh();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //delete
            delete3();
            refresh();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //edit
            try
            {
                aa = listView3.SelectedItems[0].SubItems[0].Text;
                ff = listView3.SelectedItems[0].SubItems[5].Text;
                ee = listView3.SelectedItems[0].SubItems[4].Text;
                Form4 form = new Form4(update3,_databaseConnection);
                form.setText(listView3.SelectedItems[0].SubItems[0].Text, listView3.SelectedItems[0].SubItems[1].Text, listView3.SelectedItems[0].SubItems[2].Text,
                    listView3.SelectedItems[0].SubItems[3].Text, listView3.SelectedItems[0].SubItems[4].Text, listView3.SelectedItems[0].SubItems[5].Text);
                form.ShowDialog();
                refresh();
            }
            catch (Exception exception)
            {
                MessageBox.Show("You have to select item!", "Warnning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void listView4_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                message.From = new MailAddress("treakpis95@gmail.com");
                message.To.Add(new MailAddress($"{listView4.SelectedItems[0].SubItems[4].Text}"));
                message.Subject = "Przypomnienie o zapłacie";
                message.Body = "Content";

                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("treakpis95@gmail.com", "dupa.123");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("err: " + ex.Message);
            }
        }

    }
}
