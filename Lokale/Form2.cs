﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lokale
{
    public partial class Form2 : Form
    {
        public delegate void addTo(string a, string b, string c);

        private addTo c = null;
        public Form2(addTo test)
        {
            InitializeComponent();
            c = test;
        }

        public void setText(string a, string b, string c)
        {
            textBox1.Text = a;
            textBox2.Text = b;
            textBox3.Text = c;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            c(textBox1.Text, textBox2.Text, textBox3.Text);
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
