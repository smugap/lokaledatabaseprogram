﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Lokale
{
    public partial class Form4 : Form
    {
        public delegate void addTo(string a, string b, string c, string d, string e, string f);
        private addTo c = null;
        private MySqlConnection _databaseConnection;
        
        public Form4(addTo test,MySqlConnection conn)
        {
            InitializeComponent();
            c = test;
            _databaseConnection = conn;
            downloadDataToComboBox();
        }
        public void setText(string a, string b, string c, string d, string e, string f)
        {
            textBox1.Text = a;
            textBox2.Text = b;
            textBox3.Text = c;
            textBox4.Text = d;
           
            comboBox2.SelectedIndex = comboBox2.FindString(f);
    
            comboBox1.SelectedIndex = comboBox1.FindString(e);
            
        }

        private void downloadDataToComboBox()
        {
            string query2 = $"SELECT budynki.ulica,budynki.nr_budynku,budynki.id_budynku FROM `budynki`;";
            string query = $"SELECT mieszkancy.id_mieszkanca, mieszkancy.nazwisko, mieszkancy.imie FROM `mieszkancy`;";
            MySqlCommand commandDatabase = new MySqlCommand(query2, _databaseConnection) { CommandTimeout = 60 };

            MySqlDataReader reader;
            try
            {
                _databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    comboBox2.Items.Clear();
                    while (reader.Read())
                    {
                        comboBox2.Items.Add(reader.GetString(2) + " | " +reader.GetString(0)+" " + reader.GetString(1));
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                commandDatabase = new MySqlCommand(query, _databaseConnection) { CommandTimeout = 60 };
                reader.Dispose();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    comboBox1.Items.Clear();
                    comboBox1.Items.Add(" ");
                    while (reader.Read())
                    {
                        comboBox1.Items.Add(reader.GetString(0) + " | " + reader.GetString(1) + " " + reader.GetString(2));
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                _databaseConnection.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"^\d+");
            string x = regex.Match(comboBox1.SelectedItem.ToString()).Value;
            c(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text,x , regex.Match(comboBox2.SelectedItem.ToString()).Value);
            
            this.Close();
        }
    }
}
